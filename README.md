# hppc - Hockey Pass Photo Cropper

Beschneide Fotos und speichere sie als JPG im Format 120 x 160 Pixel, wie man es für Hockeypässe braucht.

Es findet keine Netzwerkkommunikation statt. Alles passiert im Browser und funktioniert offline.

[Hier testen!](https://leindirk.gitlab.io/hppc/HockeyPassPhotoCropper.html)

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```
