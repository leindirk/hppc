const HtmlWebpackPlugin = require("html-webpack-plugin");
const InlineChunkHtmlPlugin = require("react-dev-utils/InlineChunkHtmlPlugin");

const version = require("./package.json").version;

if (process.env.NODE_ENV === "production") {
  module.exports = {
    productionSourceMap: false,
    css: {
      extract: false,
    },
    configureWebpack: {
      plugins: [
        new HtmlWebpackPlugin({
          inject: "body",
          template: "public/index.html",
          filename:
            "../gitlab_pages/HockeyPassPhotoCropper-v" + version + ".html",
        }),
        new HtmlWebpackPlugin({
          inject: "body",
          template: "public/index.html",
          filename: "../gitlab_pages/HockeyPassPhotoCropper.html",
        }),
        new InlineChunkHtmlPlugin(HtmlWebpackPlugin, [/.*/]),
      ],
    },
  };
}
